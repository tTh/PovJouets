
/*
 *	un gros chantier, vraiment gros (cTMr)
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/rails.inc"
#include	"incs/grue.inc"

object 
{
union
	{
	object { Wagon_Grue translate y*2 }
	object { Un_Rail_Droit }
	object { Un_Rail_Droit translate z*100 }
	object { Un_Rail_Droit translate z*200 }
	}
}

/*------------------------------------------------------------------*/

camera
	{
	location	<42, 12, 25>
	right		image_width/image_height*x
	look_at		<0, 6, 3>
	angle		42
	}

light_source { <350, 425, -500> color Green }
light_source { <70, 38, 40> color White }

/*------------------------------------------------------------------*/
