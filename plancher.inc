/*
		un beau plancher bien flashy
		pour mettre mes beaux wagons

*/

#declare Plancher_0 = object
{
plane	{
	y, 0
	pigment	{
		image_map { png "picz/plancher.png" interpolate 4 }
		rotate 		x*90
		translate 	<-0.5, 0, -0.5>
		scale		<30, 1, 30>
		}
	}
}

#declare Plancher_1 = object
{
plane	{
	y, 0
	pigment	{
		image_map { png "picz/plancher.png" interpolate 4 }
		rotate 		x*90
		translate 	<-0.5, 0, -0.5>
		scale		<30, 1, 30>
		}
	}
}
