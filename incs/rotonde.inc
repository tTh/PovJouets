/*
	la rotonde ou on gare les trains
*/

#declare Largeur_Porte = 22;


#declare Rotonde_mur_exterieur = object
{
difference
	{
	cylinder { <0, 0, 0>, <0, 40, 0>, 403 }
	cylinder { <0, -1, 0>, <0, 41, 0>, 397 }
	}
}

#declare Rotonde_trou_porte = object
{
box	{ <-4, -1, -Largeur_Porte>, <4, 45, Largeur_Porte> }
}

#declare Rotonde_mur_interieur = object
{
difference
	{
	cylinder { <0, 0, 0>, <0, 40, 0>, 203 }
	cylinder { <0, -1, 0>, <0, 41, 0>, 197 }
	#declare  foo = 0;
	#while (foo<189)
		object { Rotonde_trou_porte translate x*200 rotate y*foo }
		#declare foo = foo + 45;
	#end
	}
}


#declare Rotonde_1_arche = object
{
union
	{
	box { <-2, 0, -Largeur_Porte>, < 2, 45, -9> }
	box { <-2, 0,  Largeur_Porte>, < 2, 45,  9> }
	}

}

#declare Rotonde_les_arches = object
{
union
	{
	#declare  foo = 0;
	#while (foo<189)

		object { Rotonde_1_arche translate x*191 rotate y*foo }

		#declare foo = foo + 45;

	#end
	}

}

#declare Rotonde_0 = object
{

union
	{
	object { Rotonde_mur_interieur pigment { color Blue } }
	object { Rotonde_mur_exterieur pigment { color Red } }
	object { Rotonde_les_arches pigment { color Green } }
	
	object { Rotonde_1_arche pigment { color Cyan } rotate y*90 }
	}
}
