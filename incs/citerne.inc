/*
 *		LES JOUETS DE TONTON TH
 *		Une citerne de St Chinian
 */
//-----------------------------------------------------------------

#include  "incs/constantes.inc"

#declare Diam_Citerne = 4.6;
#declare Epp_Cerclage = 0.233;
#declare Diam_Cerclage = (Diam_Citerne+Epp_Cerclage);
#declare Larg_Cerclage = 0.6;
#declare Off_Cerclage = 3;

#declare Citerne = object
{
union	{
	intersection
		{
		sphere		{ <0, 0, 0>, 11 }
		cylinder	{ <0, 0, -15>, <0, 0, 15>, Diam_Citerne }
		}

	/*
	 *	les deux cerclages
	 */
	cylinder { <0, 0, -5>, <0, 0, -5.8>, Diam_Cerclage }
	torus { Diam_Cerclage-Epp_Cerclage Epp_Cerclage
			 rotate x*90 translate z*-5 }
	torus { Diam_Cerclage-Epp_Cerclage Epp_Cerclage
			 rotate x*90 translate z*-5.8 }
	cylinder { <0, 0,  5>, <0, 0,  5.8>, Diam_Cerclage }
	torus { Diam_Cerclage-Epp_Cerclage Epp_Cerclage
			 rotate x*90 translate z*5 }
	torus { Diam_Cerclage-Epp_Cerclage Epp_Cerclage
			 rotate x*90 translate z*5.8 }

	cylinder { <0, 4, 0>, <0, 5.7, 0>, 1.8 }
	difference
		{
		cylinder { <0, 5.6, 0>, <0, 6.4, 0>, 2.4 }
		cylinder { <0, 5.5, 0>, <0, 6.5, 0>, 1.4 }
		}
	}
scale <1, 0.882, 1>
translate y*10.7			// ???
texture		{ Rouge_Train_1 }
}

//-----------------------------------------------------------------
#declare Wagon_Citerne = object
{
union
	{
	object { Base_Courte }
	object { Citerne }
	}
}
//-----------------------------------------------------------------
