/*
		LE TRAIN MINIATURE DE TONTON TH
*/

#macro galerie(Larg, Long, Diam, Coin)

#local Larg2 = Larg / 2.0;
#local Long2 = Long / 2.0;

union
	{
	cylinder { <-Larg2, 0, -Long2>, < Larg2, 0, -Long2>, Diam }
	cylinder { <-Larg2, 0,  Long2>, < Larg2, 0,  Long2>, Diam }
	cylinder { <-Larg2, 0, -Long2>, <-Larg2, 0,  Long2>, Diam }
	cylinder { < Larg2, 0, -Long2>, < Larg2, 0,  Long2>, Diam }
	sphere { <-Larg2, 0, -Long2>,  Diam }
	sphere { <-Larg2, 0,  Long2>,  Diam }
	sphere { <-Larg2, 0, -Long2>,  Diam }
	sphere { < Larg2, 0, -Long2>,  Diam }
	}

#end
