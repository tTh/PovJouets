/*
 *		LES JOUETS DE TONTON TH
 *		le wagon-ridelles
 */
//-----------------------------------------------------------------

#declare Ridelle_Cote = object
{
union	{
	cylinder { <0, 4.5, -11>, <0, 4.5, 11>, 0.25 }
	box { <-.25, 4, -11>, <.25, 4.5, 11> }
	#declare foo=0;
	#while (foo<6)
		box { <-.4, 0, foo*4-10-.2>, <.4, 4, foo*4-10+.2> }
		#declare foo=foo+0.5;
	#end
	}
texture {
	pigment { color MediumWood }
	finish { roughness .83 }
	}
translate y*5.85
}

/* avant et arriere */

#declare Ridelle_Bout = object
{
box { <-4, 6.2, -0.1>, <4, 9.7, 0.1> }
texture {
	pigment { color MediumWood }
	finish { roughness .73 }
	}
}

//-----------------------------------------------------------------

#declare Wagon_Ridelles = object
{
union
	{
	object { Base_Courte }
	object { Ridelle_Cote translate x*3.5 }
	object { Ridelle_Cote translate x*-3.5 }
	object { Ridelle_Bout translate z*11.3 }
	object { Ridelle_Bout translate z*-11.3 }
	}
}

//-----------------------------------------------------------------
