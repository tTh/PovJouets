/*
			poteaux.inc
			-----------

*/

#include   "woods.inc"

// -----------------------------------------------------------------

#declare Poteau_0 = object
{
union
	{
	box { <-2.2, 0, -2.2>, <2.2, 0.8, 2.2> }
	cylinder { <0, 0, 0>, <0, 20, 0>, 0.75 }
	}
texture { New_Penny scale 0.3 }
}

// -----------------------------------------------------------------

#declare Base_Poteau_1 = object
{
union
	{
	box { <-1, 0, -1>, <-0.08, 1.6, 1.5> }
	box { <0.08, 0, -1.5>, <1, 1.6, 1> }
	}
texture
	{
	pigment { color Gray75 }
	normal { bumps 0.333 }
	}
}

#declare Bois_Poteau_1 = object
{
union
	{
	cylinder { <0, 0, 0>, <0, 20, 0>, 0.42 }
	cone 	{ <0, 20, 0>, 0.64, <0, 21, 0>, 0.21 }
	cylinder { <-4, 19, 0>, <4, 19, 0>, 0.20 }
	}
texture	{ T_Wood12 rotate x*90 scale 3 }
}
	
#declare Poteau_1 = object
{
union
	{
	object { Base_Poteau_1 }
	object { Bois_Poteau_1 }
	}
}

// -----------------------------------------------------------------
