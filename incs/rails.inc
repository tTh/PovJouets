/*
		les rails, a la demande de Nicolas
		----------------------------------

	Et bien, c'est pas si facile que c,a a faire...
*/

#include "woods.inc"
#include "metals.inc"

#include "incs/constantes.inc"

/* ---------------------------------------------------------------- */

#declare Une_Traverse = object
{
box { <-8, 0, -1>, <8, 1, 1> }
texture { T_Wood7 rotate <3, 90, 2> }
}

/* ---------------------------------------------------------------- */

#declare Rail_Droit = object
{
difference
	{
	box { <-.4, 1, -50>, <.4, 2, 50> }
	cylinder { <-.4, 1.6, -51>, <-.4, 1.6, 51>, .25 }
	cylinder { < .4, 1.6, -51>, < .4, 1.6, 51>, .25 }
	}

texture
	{
	pigment { color rgb<0.3, 0.5, 0.7> }
	finish  { metallic 0.80 }
	normal  { bumps 0.70 scale 0.5 }
	}
}

/* ---------------------------------------------------------------- */
/* new 7 janvier 2007 */
#declare Tire_Fond = object
{
union	{
	cylinder { <0, 0, 0>, <0, 0.1, 0>, 0.33 }
	box { <-0.18, 0.099, -0.18>, <0.18, 0.2, 0.18> }
	}
texture	{
	pigment { color Gray25 }
	finish { metallic 0.95 }
	}
}
/* ---------------------------------------------------------------- */

#declare Un_Rail_Droit = object
{
#declare ecart = (ECARTEMENT/2) ;
// #fopen LOG "log.tirefond" write
union
	{
	object { Rail_Droit translate <-ecart, 0, 0>  }
	object { Rail_Droit translate < ecart, 0, 0>  }
	#declare foo = -9;
	#while (foo <= 9)
		// #write (LOG, foo, "  ")

		#declare bar = foo * 5.15;
		object { Une_Traverse translate z*bar }
		object { Tire_Fond rotate y*(rand(RND)*360)
					translate  <-(ecart+1), 1.1, bar> }
		object { Tire_Fond rotate y*(rand(RND)*360)
					 translate  < (ecart+1), 1.1, bar> }

		#declare rangle = (rand(RND)*360);
		// #write (LOG, rangle, "  ")
		object { Tire_Fond rotate y*rangle
					translate  <-(ecart-1), 1.1, bar> }
		#declare rangle = (rand(RND)*360);
		// #write (LOG, rangle, "  ")
		object { Tire_Fond rotate y*rangle
					translate  < (ecart-1), 1.1, bar> }

		#declare foo = foo+1;
		// #write (LOG, "\n")
	#end
	}
// #fclose LOG
}

/* ---------------------------------------------------------------- */
