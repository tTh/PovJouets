/*
		Un wagon de voyageurs
		---------------------

*/

#declare Chassis_Corail = object
{
union	{
	box	{ <-4, 5.5, -20>, <4, 6.5, 20> }
	box	{ <-3.5, 3.5, -5>,  <3.5, 5.4, -1> }
	box	{ <-3.5, 3.5,  5>,  <3.5, 5.4,  1> }
	}
texture	{ Jaune_Corail }
}

#declare Base_Corail = object
{
union	{
	object { Chassis_Corail }
	object { Boogie	translate z*-13.8 }
	object { Boogie	translate z* 13.8 }
	}
// finish	{ phong 0.65 }
}


#declare LARG_FEN = 5;
#declare HAUT_FEN = 4;
#declare E_FEN = 0.5;

#declare Bord_Fenetre = object
{
difference {
	box { <-1, -HAUT_FEN, -LARG_FEN>,
					<1, HAUT_FEN, LARG_FEN> }
	box { <-2, -HAUT_FEN+E_FEN, -LARG_FEN+E_FEN>,
					<2, HAUT_FEN-E_FEN, LARG_FEN-E_FEN> }
	}
texture { T_Chrome_2C }
}

#declare Cabine_Corail = object
{
difference {
	box	{ <-4.5, 6.6, -20>, <4.5, 15, 20> }
	box	{ <-4, 6.6, -19.5>, <4, 15.1, 19.5> }

	#declare foo = -4;
	#while (foo <= 4)
		#declare px = foo * 4.16;
		box { <-5, 9.42, px-1.7>, <5, 13.5, px+1.7> }
		#declare foo = foo + 1;
	#end

	/*
	 *   c'est quoi, ce truc ?
	 *	ben, les portes des deux bouts !
	 */
	box	{ <-2.2, 7, -21>, < 2.2, 14.8, -19> }
	box	{ <-2.2, 7,  21>, < 2.2, 14.8,  19> }
	}
texture { Orange_Corail }
}

#declare Toit_Corail = object
{
intersection {
	box	{ <-4.7, 0, -21>, <4.7, 14, 21> }
	cylinder { <0, -11, -22>, <0, -11, 22>, 12.5 } 
	}
translate y*15
texture {
	pigment { color Red }
	finish	{ phong 0.6 }
	}
}

//
//	la tuyauterie sur les deux caissons d'en dessous
//
#declare Citerne_Corail = object
{
#local Ltc = 5.22;
#local Rtc = 1.12;
#local Etc = 4.98;

merge	{
	cylinder { -Ltc*z, Ltc*z, Rtc }
	sphere   { -Ltc*z,        Rtc }
	sphere   {  Ltc*z,        Rtc }
	}
scale <1, 1, 0.707>
texture { T_Chrome_4C }
}
#declare Tuyaux_Corail = object
{
union	{
	object { Citerne_Corail   translate -x* Etc	}
	object { Citerne_Corail   translate  x* Etc	}
	}
}

//	------------------------------------------

#declare Armature_Siege_Corail = object
{
union	{
	cylinder { <-1.3, 0, 0>, <-1.3, 1.50, 0>, 0.2 }
	cylinder { < 1.3, 0, 0>, < 1.3, 1.50, 0>, 0.2 }

	cylinder { <-1.3, 0, 0>, <-1.3, .10, 0>, 0.5 }
	cylinder { < 1.3, 0, 0>, < 1.3, .10, 0>, 0.5 }

	cylinder { <-2.1, 1.03, 0>, < 2.1, 1.03, 0>, 0.096 }

	/*
	 *	les deux barres qui tiennent les dossiers
	 */
	cylinder { <-1.3, 0.9, 0.9>, <-1.3, 3.7, 0.9>, 0.068 }
	cylinder { < 1.3, 0.9, 0.9>, < 1.3, 3.7, 0.9>, 0.068 }

	sphere { <-1.3, 3.7, 0.9>, 0.068 }
	sphere { < 1.3, 3.7, 0.9>, 0.068 }
	}
texture { T_Chrome_2C }
}

#declare Coussins_Siege_Corail = object
{
union	{
	box { <-2, 1.75, -1>, <2, 1.40, 1> }
	box { <-2, 1.95,  1>, <2, 3.10, 0.8> }
	
	cylinder { <-2, 3.38,  0.9>, <2, 3.38, 0.9>, 0.2 }
	}
texture {
	pigment { color Brown }
	normal  { bumps 0.6 scale 0.42 }
	finish	{ phong 0.4 diffuse 0.3 }
	}
}

#declare Siege_Corail = object
{
union	{
	object { Armature_Siege_Corail }
	object { Coussins_Siege_Corail }
	}
}

//	------------------------------------------
//
//	avant de faire les lumieres, il faut faire l'ampoule.
//
#declare Ampoule_Corail = object
{
sphere { <0, 0, 0> 0.17 pigment { color White } }
}

//	------------------------------------------

#declare Lumieres_Corail = object
{
union	{
	#declare foo = -2;
	#while (foo <= 2)
		#declare px = foo * 6.97;

		// sphere { <0, 15.3, px> 0.6 pigment { color BakersChoc } }

		light_source {
			<0, 0, px> color Gray35
			fade_distance 6 fade_power 1
			looks_like { Ampoule_Corail }
			translate y*13.85
			}

		#declare foo = foo + 1;
	#end
	}

}

#declare Interieur_Corail = object
{
union	{
	#declare foo = -4;
	#while (foo <= 4)
		#declare px = foo * 4.16;
		object { Siege_Corail translate <1.5, 6.6, px> }
		#declare foo = foo + 1;
	#end
	}
}
//	------------------------------------------
/*
 *		MAIS CETTE RAMPE EST COMPLETEMENT FAUSSE !!!
 */
#declare Rampe = object
{
union	{
	#declare foo = -2;
	#while (foo <= 2)
		#declare px = foo * 6.97;

		sphere { <0, 15.3, px> 0.6 pigment { color BakersChoc } }

		light_source {
			<0, 0, px> color Gray25
			fade_distance 6 fade_power 1
			looks_like { Ampoule_Corail }
			translate y*13.8
			}

		#declare foo = foo + 1;
	#end
	}
}
//	------------------------------------------

#declare Wagon_Corail = object
{
union	{
	object { Base_Corail }
	object { Cabine_Corail }
	object { Toit_Corail }
	object { Tuyaux_Corail translate y*4.16 }
	object { Interieur_Corail }
	object { Lumieres_Corail }
	// object { Rampe }
	}
}

