/*
		les elements du decor
		---------------------

	voir aussi le fichier 'rails.inc'

*/

/* ------------------------------------------------------------------------ */

#declare Balise_0 = object
{
union
	{
	cylinder {
		<0, 0, 0>, <0, 6.01, 0>, 1.2
		pigment { color Red }
		}
	cylinder {
		<0, 6, 0>, <0, 8, 0>, 1
		pigment { color White }
		}
	sphere {
		<0, 8, 0>, 1
		pigment { color Red }
		}
	}
finish  { phong 0.7 }
}

/* ------------------------------------------------------------------------ */

#declare Banc_0 = object
{
union
	{
	box { <-6, 3, -1>, <6, 4, 1> }
	box { <-6, 4.4, -1.05>, <6, 6, -0.95> }
	}
texture {
	pigment { Cyan }
	finish { phong 0.7 }
	}
}

/* ------------------------------------------------------------------------ */

