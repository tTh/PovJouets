/*
		UNE LOCOMOTIVE ELECTRIQUE
		-------------------------
	necessite l'inclusion des elements communs 'train.inc'
*/

#include   "incs/instruments.inc"

#declare Base_Loco_Electric = object
{
#local DYR = 3;
union  {
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, DYR, -12> }
	object { Petite_Roue 			translate < 5.5, DYR, -12> }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, DYR,  -5> }
	object { Petite_Roue 			translate < 5.5, DYR,  -5> }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, DYR,   5> }
	object { Petite_Roue 			translate < 5.5, DYR,   5> }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, DYR,  12> }
	object { Petite_Roue 			translate < 5.5, DYR,  12> }
	}
}

#declare Chassis_Electric = object
{
box { <-4.5, 1, -10>, <4.5, 2, 10> }
texture {
	pigment { color BlueViolet }
	finish	{ phong 0.6 }
	}
}

#declare Cabine_Loco_Electric = object
{
union {
	box	{ <-4, 4, -11>, <4, 10, 11> }
	cylinder { <-3, 10, -11>, <-3, 10, 11> 1 }
	cylinder { < 3, 10, -11>, < 3, 10, 11> 1 }

	cylinder { <0, 5, -11>, <0, 10, -11>, 4 }
	difference {
		cylinder { <0, 3, -11>, <0, 11, -11>, 3 }
		cylinder { <0, 5, -11>, <0, 11.1, -11>, 2.8 }
		}
	torus	{ 3, 1 translate <0, 10, -11> }

	cylinder { <0, 5,  11>, <0, 10,  11>, 4 }
	difference {
		cylinder { <0, 3,  11>, <0, 11,  11>, 3 }
		cylinder { <0, 5,  11>, <0, 11.1,  11>, 2.8 }
		}
	torus	{ 3, 1 translate <0, 10,  11> }
	}

texture {
	pigment { color DarkGreen }
	finish	{ phong 0.8 }
	}
}

// -------------------------------------------------------------------

#declare Haut_Pento = 18;

#declare Base_Panto_0 = object
{
union {
	cylinder { <0, 10, 0>, <0, 10.1, 0>, 1 }
	cylinder { <0, 10, 0>, <0, Haut_Pento, 0>, .155 }
	cylinder { <-2, Haut_Pento, 0>, <2, Haut_Pento, 0>, .155 }
	sphere { <-2, Haut_Pento, 0>, .31 }
	sphere { < 2, Haut_Pento, 0>, .31 }
	}
}

#declare Bases_Panto = object
{
union {
	object { Base_Panto_0 translate z*2.9 }
	object { Base_Panto_0 translate z*-2.9 }
	}
texture { T_Copper_2C }
}
// -------------------------------------------------------------------

#declare Armature_Cockpit = object
{
union {
	torus { 3, 0.12  rotate <90, 0, 0> }
	torus { 3, 0.12  rotate <90, 120, 0> }
	torus { 3, 0.12  rotate <90, 240, 0> }
	difference
		{
		cylinder { <0, 0, 0>, <0, .2, 0>, 3.2 }
		cylinder { <0, -0.1, 0>, <0, .21, 0>, 2.8 }
		}
	cylinder { <0, 2.95, 0>, <0, 3.12, 0>, .8 }
	}
texture { T_Chrome_2C }
}

#include	"textures.inc"

#declare Bulle_Cockpit = object
{
difference {
	sphere { <0, 0, 0>, 3.000 }
	sphere { <0, 0, 0>, 2.996 }
	}
interior { I_Glass }
texture { Glass2 }
finish { reflection 0.2 }
}

#declare Cockpit = object
{
difference
	{
	union
		{
		object { Armature_Cockpit }
		object { Bulle_Cockpit }
		}
	box { <-5, -0.1, -5>, <5, -5, 5> }
	}
scale <1, 0.8, 1>
translate y*11
}

// -------------------------------------------------------------------
#declare Les_Phares = object
{
union
	{
	object { Phare_0 translate < 3.6, 8, -15.7> }
	object { Phare_0 translate <-3.6, 8, -15.7> }
	box	{
		<3.8, 7.9, -14.5>, <-3.8, 8.1, -14>
		texture { T_Chrome_3A }
		}
	}
}
// -------------------------------------------------------------------

#declare Locomotive_Electric = object
{
union
	{
	object { Base_Loco_Electric }
	object { Cabine_Loco_Electric }
	object { Cockpit translate z*11 }
	object { Cockpit translate z*-11 }
	object { Bases_Panto }
	object { Les_Phares }
	object { Chassis_Electric }
	}
}

//-------------------- yo ! -------------------------------------------

