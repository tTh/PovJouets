/*
		Une grue pour soulever les wagons
		---------------------------------

*/

#declare Z_Pivot = 11;

#declare H_Cabine = 6;

#declare A_Pivot = 42;
#declare A_Fleche = 30;

#declare Chassis_Grue = object
{
union
	{
	box	{ <-4, 5.5, -20>, <4, 6.5, 20> }
	box	{ <-3.5, 3.2, -5>,  <3.5, 5.4, 5> }
	}
pigment { color Brown }
}

#declare Base_Grue = object
{
union
	{
	object { Chassis_Grue }
	object { Boogie	translate z*-13.8 }
	object { Boogie	translate z* 13.8 }
	}
finish	{ phong 0.5 }
}

// ----------------------------------------------------------------------

#declare Pivot_Grue = object
{
union
	{
	cylinder { < 0, 0.0, 0>, <0, 0.5, 0>, 3.3 }
	cylinder { < 0, 0.5, 0>, <0, 1.0, 0>, 3 }
	cylinder { < 0, 1.0, 0>, <0, 1.5, 0>, 3.3 }
	}
pigment { color Orange }
}

//	------------------------------------------


#declare Plateau_Cabine_Grue = object
{
box { <-5, 0, -5>, <5, 0.5, 5> }
pigment { color Yellow }
}

#declare Boite_Cabine_Grue = object
{
difference
	{
	box { <-3, 0, -3>, <3, H_Cabine, 3> }
	box { <-2.8, 0, -2.9>, <2.9, H_Cabine+0.1, 2.8> }
	box { <-3.1, 3.6, -2.6>, <3.1, H_Cabine-0.2, -0.3> }
	box { <-3.1, 3.6,  2.6>, <3.1, H_Cabine-0.2,  0.3> }
	box { <-2.6, 3.6, -3.1>, <-0.3, H_Cabine-0.2, 3.1> }
	box { < 2.6, 3.6, -3.1>, < 0.3, H_Cabine-0.2, 3.1> }
	}
pigment { color Yellow }
}

#declare Toit_Cabine_Grue = object
{
intersection
	{
	box	{ <-3.2, 0, -3.4>, <3.2, 4, 3.2> }
	cylinder { <0, -12, -3.5>, <0, -12, 3.3>, 12.6 } 
	}
translate y*H_Cabine
pigment { color Orange }
}

#declare Cabine_Grue = object
{
union
	{
	object { Plateau_Cabine_Grue }
	object { Boite_Cabine_Grue }
	object { Toit_Cabine_Grue }
	}
translate y*1.5
}
//	------------------------------------------

#declare Appui_Fleche = object
{
difference
	{
	union
		{
		box		{ <-1.5, 0, -1>, <1.5, 1, 1> }
		box		{ <-1.4, 0.99, -0.8>, <1.4, 1.7, 0.8> }
		cylinder	{ <-1.4, 1.7, 0>, <1.4, 1.7, 0>, 0.8 }
		}
	cylinder	{ <-0.85, 1.7, 0>, <0.85, 1.7, 0>, 0.9 }
	}
texture {
	pigment { color Red } 
	finish  { ambient 0.5 }
	}
}

#declare Pivot_Fleche = object
{
union
	{
	cylinder	{ <-0.8, 1.7, 0>, <0.8, 1.7, 0>, 0.7 }
	cylinder	{ <-1.55, 1.7, 0>, <1.55, 1.7, 0>, 0.51 }
	}
pigment { color Cyan }
}

#declare Bras_Fleche = object 
{
union
	{
	box { <-0.5, -0.5, -6>, <0.5, 0.5, -1> }
	box { <-0.4, -0.4, -5>, <0.4, 0.4, -11> }
	box { <-0.3, -0.3, -10>, <0.3, 0.3, -15> }
	box { <-0.2, -0.2, -9>, <0.2, 0.2, -20> }
	}
pigment { color Yellow } 
rotate x*A_Fleche
translate y*1.7
}

#declare Fleche = object
{
union
	{
	object { Appui_Fleche		translate <3.5, 0, 0> }
	object { Pivot_Fleche		translate <3.5, 0, 0> }
	object { Bras_Fleche		translate <3.5, 0, 0> }
	}
translate y*2.0		/* pour la poser sur la plate forme */
}
//	------------------------------------------
#declare Corps_Grue = object
{
union	{
	object { Pivot_Grue rotate y*42  }
	object { Cabine_Grue }
	object { Fleche }
	}
translate y*7.0
}
//	------------------------------------------

#declare Wagon_Grue = object
{
union
	{
	object { Base_Grue }
	object { Corps_Grue }
	}
}

//	------------------------------------------
