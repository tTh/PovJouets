/*
		LES POVJOUETS DE TONTON TH
		Quelque constantes utiles
*/

#ifdef(Jouets_Constantes)
// do nothing
// #warning "*** constantes deja definies\n"
#else
#declare Jouets_Constantes=version;

/* ------------------------------------------------------------ */
/* constantes geometriques					*/

#declare ECARTEMENT	= 12;
#declare H_RAIL		= 2;

/*  generateur de nombres pseudo-aleatoires.  
    mais comment le 'seeder' de fac,on differente a chaque run ? */

#declare RND = seed(42);

#declare FADE_DIST = 2.0;
#declare FADE_POWA = 2.0;

/* ------------------------------------------------------------ */
/* 	il nous faut quelques textures 'standards'		*/

#declare Orange_Corail = texture
{
pigment { color Orange }
normal  { dents 0.42 scale 0.042 }
finish	{ phong 0.5 }
}

#declare Jaune_Corail = texture
{
pigment { color Yellow }
normal  { dents 0.32 scale 0.242 }
finish	{ phong 0.5 }
}

#declare Vert_Train_1 = texture
{
// pour la benne
	pigment { color Green*0.70 }
	normal { bumps 0.17 rotate <10, 0, 10> scale 0.42 }
	finish  { phong 0.5 }
}

#declare Rouge_Train_1 = texture
{
// pour la citerne
	pigment { color rgb <0.96, 0.25, 0.2> }
	normal { bumps 0.33 scale 0.42 }
	finish  { phong 0.5 }
}

#declare Texture_Roues = texture
{
	pigment { color LightSteelBlue }
	finish { specular 0.5 }
}

#declare Texture_Chassis = texture
{
	pigment { color Yellow }
	finish { phong 0.5 ambient 0.4 }
}


/* ------------------------------------------------------------ */
#end		// #ifdef

