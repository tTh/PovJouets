/*
 *		des éléments du petit train 
 *		---------------------------
 *
 */

//-----------------------------------------------------------------
#include "incs/constantes.inc"

//-----------------------------------------------------------------

#declare Corps_Grande_Roue = object
{
difference {
	union {
		cylinder { < 0,   0, 0>, <0.8, 0, 0>, 4 }
		cylinder { <-0.5, 0, 0>, <0,   0, 0>, 4.3 }
		}
	cylinder { <-1.2, 0, 0> <-0.6, 0, 0>, 2.8 }
	cylinder { < 1.2, 0, 0> < 0.6, 0, 0>, 3.2 }

	#declare foo = 0;
	#while (foo < 360)
		#local R = 2.35;
		#declare xx = sin(radians(foo))*R;
		#declare yy = cos(radians(foo))*R;

		cylinder { <-2, xx, yy>, <2, xx, yy>, 0.56 }

		#declare foo = foo + 36;
	#end
	}
texture	{ Texture_Roues }
}

//-----------------------------------------------------------------

#declare Corps_Petite_Roue = object
{
difference {
	union {
		cylinder { < 0, 0, 0>, <0.8, 0, 0>, 3 }
		cylinder { <-0.5, 0, 0>, <0, 0, 0>, 3.35 }
		}
	cylinder { <-1.4, 0, 0> <-0.6, 0, 0>, 1.7 }
	cylinder { < 1.4, 0, 0> < 0.6, 0, 0>, 1.7 }

	#declare foo = 0;
	#while (foo < 360)
		#declare xx = sin(radians(foo))*2.29;
		#declare yy = cos(radians(foo))*2.29;

		sphere { <1.1, xx, yy>, 0.44 }

		#declare foo = foo + 36;
	#end
	}
texture	{ Texture_Roues }
}

//-----------------------------------------------------------------

#declare Moyeu = object
{
union {
	intersection {
		sphere { <0, 0, 0>, .69 }
		box    { <0, -2, -2>, <2, 2, 2> }
		}
	cylinder { <-3, 0, 0>, <0.01, 0, 0>, .33 }
	}
texture { T_Chrome_2C }
}

//-----------------------------------------------------------------

#declare Petite_Roue = object
{
union {
	object { Corps_Petite_Roue }
	object { Moyeu translate <0.95, 0, 0> }
	}
}

#declare Grande_Roue = object
{
union {
	object { Corps_Grande_Roue }
	object { Moyeu translate <0.95, 0, 0> }
	}
}

//-----------------------------------------------------------------

#declare Chassis_Court = object
{
union	{
	box	{ <-3.5, 0.2, -12>,     <3.5, 1.8, 12> }
	box	{ <-3.6, 0.7, -12.5>, <3.6, 1.3, 12.5> }
	}
translate y*4.1
texture { Texture_Chassis }
}

#declare Chassis_Long = object
{
union	{
	box	{ <-3.5, 0.2, -15.5>, <3.5, 1.8, 15.5> }
	box	{ <-4, 0.5, -16>, <4, 1.5, 16> }
	}
translate y*4.1
texture { Texture_Chassis }
}

//-----------------------------------------------------------------

#declare Chassis_Boogie = object
{
union	{
	box	{ <-3.85, 0, -3>, <3.85, 1.6, 3> }
	cylinder { <0, 1, 0>, <0, 4, 0>, 1.5 }
	}
translate y*2
pigment { color Gray30 }
}

#declare Boogie = object
{
#local HR = 3;
union
	{
	object { Chassis_Boogie }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, HR, -3.5> }
	object { Petite_Roue			translate < 5.5, HR, -3.5> }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, HR,  3.5> }
	object { Petite_Roue			translate < 5.5, HR,  3.5> }
	}
}

//-----------------------------------------------------------------

#declare Base_Courte = object
{
#local HR = 4;
union	{
	object { Chassis_Court }
	object { Grande_Roue scale <-1, 1, 1>	translate <-5.5, HR, -8> }
	object { Grande_Roue			translate < 5.5, HR, -8> }
	object { Grande_Roue scale <-1, 1, 1>	translate <-5.5, HR,  8> }
	object { Grande_Roue			translate < 5.5, HR,  8> }
	}
}
	
//-----------------------------------------------------------------
