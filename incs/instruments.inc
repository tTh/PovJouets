/*

			instruments.inc
			---------------

*/


/* ----------------------------------------------------- instruments ------- */

#include  "glass.inc"

#ifdef (Jouets_Instruments)
	// on fait quedalle
	#warning "*** instruments deja inclus ***\r\n"
#else
	#declare Jouets_Instruments = version;

/* ----------------------------------------------------- instruments ------- */

#declare  D_Cadran = 1.000 ;

#declare Boitier_Cadran = object
{
union
	{
	cylinder { <0, 0, 1>, <0, 0, 0.2>, D_Cadran }
	cylinder { <0, 0, .3>, <0, 0, .25>, D_Cadran+.05 }
	difference
		{
		cylinder { <0, 0, 1.2>, <0, 0, 0.9>, D_Cadran+0.05 }
		cylinder { <0, 0, 1.3>, <0, 0, 0.8>, D_Cadran-0.05 }
		}
	}

texture { T_Chrome_2C }
}

/* voir le proggy 'make_cadran.f' */
#declare Fond_Cadran = object
{
difference
	{
	cylinder { <0, 0, 1.1>, <0, 0, 0.9>, D_Cadran }
	cylinder { <0, 0, 1.2>, <0, 0, 0.8>, 0.06 }
	}

pigment
	{
	image_map { png "picz/cadran.png" interpolate 2 }
	translate <-0.5, -0.5, 0>
	scale <D_Cadran*2, D_Cadran*2, 1>
	}
}

/* ----------------------------------------------------- instruments ------- */
#declare Aiguille_0 = object
{
union
	{
	// le pivot de l'aiguille
	cylinder { <0, 0, 1.07>, <0, 0, 1.22>, 0.06 }
	
	// le 'cylindre-machin' 
	cylinder { <0, 0, 1.16>, <0, 0, 1.21>, 0.15 }

	// le corps de l'aiguille
	box { <-0.2, -0.033, 1.17>, <D_Cadran-0.30, 0.033, 1.2> }
	}
texture { T_Copper_3C }
}
/* ----------------------------------------------------- instruments ------- */
#declare Cadran_0 = object
{
union
	{
	object { Boitier_Cadran }
	object { Fond_Cadran }
	object { Aiguille_0 }
	}
}
/* ----------------------------------------------------- instruments ------- */
#declare Cadran_1 = object
{
union
	{
	object { Boitier_Cadran }
	object { Fond_Cadran }
	object { Aiguille_0 }
	}
}
/* ----------------------------------------------------- instruments ------- */
/*
 *	Bon, maintenant, il est temps de mettre des lumieres a ces trains
 *	donc on va commencer par des phares pour la loco electrique.
 */

#declare Ampoule = object
{
sphere { <0, 0, 0>, .25 }
texture {
	pigment { color rgb<1.0, 1.0, 0.8> }
	finish { ambient 0.8 diffuse 0.8 }
	}
}

#declare Coque_Phare_0 = object
{

union
	{
	difference
		{
		sphere { <0, 0, 0>, 1 }
		sphere { <0, 0, 0>, .9 }
		box { <-2, -2, 0>, <2, 2, 2> }
		}
	torus { 1, .06 rotate x*90 }
	cylinder { <0, 0, -1>, <0, 0, -1.2>, .1 }
	}
texture { T_Chrome_2C }
}

#declare Vitre_Phare_0 = object
{
union
	{
	// cylinder { <0, 0, -.01>, <0, 0, .01>, 1 }
	torus { .8, .03 rotate x*90 }
	torus { .6, .03 rotate x*90 }
	torus { .4, .03 rotate x*90 }
	torus { .2, .03 rotate x*90 }
	}
interior { I_Glass }
texture { T_Glass1 }
}

#declare Phare_0 = object
{
union
	{
	object { Coque_Phare_0 }
	object { Vitre_Phare_0 }

	light_source
		{
		<0, 0, 0> color White
		looks_like { Ampoule }
		translate z*-.5
		}
	}
rotate y*180
}
/* ----------------------------------------------------- instruments ------- */
/*
		Et un beau feu rouge pour mettre a l'arriere
		de toutes ces somptueuses machines ?
*/

#declare Feu_Rouge = object
{
box { <-1, 0, -1>, <1, 4, 0> }
pigment { color Red }
}

/* ----------------------------------------------------- instruments ------- */

#debug "instruments loaded...\r\n"
#end

/* ----------------------------------------------------- instruments ------- */
