/*
 *		experiment for the futur
 */


#declare Roue = object
{
torus { .5, .2  pigment { color Gray50 } finish { phong 0.8 } }
rotate	x*90
translate y*0.75
}


#declare Voiture = object
{
union
	{
	object { Roue translate <-2, 0, -1> }
	object { Roue translate < 2, 0, -1> }
	object { Roue translate <-2, 0,  1> }
	object { Roue translate < 2, 0,  1> }
	}

}
