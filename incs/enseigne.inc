/*
 *	see 'gare.inc' for usage
 */

#include  "chars.inc"

#declare Enseigne = object
{
union
    {
    object { char_H translate x*-27.5 }
    object { char_E translate x*-22.5 }
    object { char_L translate x*-17.5 }
    object { char_L translate x*-12.5 }
    object { char_O translate x*-7.5 }

    object { char_W translate x*2.5 }
    object { char_O translate x*7.5 }
    object { char_R translate x*12.5 }
    object { char_L translate x*17.5 }
    object { char_D translate x*22.5 }
    }
texture { PinkAlabaster scale 3 }
}

/* (c) Oulala */
