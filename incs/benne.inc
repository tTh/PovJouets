/*
	17 Octobre 1999 
	Le wagon-benne a (enfin) droit a son propre fichier .INC
*/

#include "incs/train.inc"

//-----------------------------------------------------------------

#declare Petite_Benne = object
{
union {
	difference {
		box { <-3.5, 0, -10>,   <3.5, 6, 10> }
		box { <-3, 0.5, -9.5>, <3, 7, 9.5> }
		}
	cylinder { <0, 4.5, -9.75>, <0, 4.5, -12>, .8 }
	cylinder { <0, 4.5,  9.75>, <0, 4.5,  12>, .8 }
	}
translate y*6
texture { Vert_Train_1 }
}

//-----------------------------------------------------------------
/*
19 Oct 1999: premiere version.
10 Fev 2000: BAD CODE HERE
*/
#declare Support_Benne = object
{
union {
	difference {
		box { <-3, 0, -0.5>, <3, 5, 0.5> }
		cylinder { <0, 5, -.75>, <0, 5, .75>, 1.1 }
		box { <-2.3, 0.36, -0.6>, <2.3, 3.4, 0.6> }
		}
	cylinder { <0, 5, -.75>, <0, 5, .75>, 1.33 }
	}

translate y*6
texture {
	pigment { color Brown }
	finish  { phong 0.6 }
	}
}

//-----------------------------------------------------------------
#declare Articulation_Benne = object
{
difference
	{
	cylinder { <0, 5, -.5>, <0, 5, .5>, 1.15  }
	cylinder { <0, 5, -.6>, <0, 5, .6>, 0.85 }
	}
translate y*6
texture { T_Chrome_2D }
}
//-----------------------------------------------------------------

#declare Piston_Benne = object
{
union
	{
	cylinder { <5, 4, 0>, <5, 12, 0>, 0.4 }
	sphere { <5, 4, 0>, 0.4 }
	sphere { <5, 12, 0>, 0.4 }
	}
texture { T_Chrome_2C }
}

//-----------------------------------------------------------------

#declare Support_Piston_Benne = object
{
union
	{
	cylinder { <0, -1.1, 0>, <0, 1.1, 0>, 0.65 }
	box { <-3, -0.3, -1.42>, <2, 0.4, 1.42> }
	}
translate <5, 5, 0> 
texture {
	pigment { color Brown }
	finish  { phong 0.412 }
	}
}

//-----------------------------------------------------------------

#declare Wagon_Benne = object
{
union
	{
	object { Base_Courte }
	object { Petite_Benne translate y*0.55 }
	object { Support_Benne translate z*-11 }
	object { Support_Benne translate z* 11 }
	object { Piston_Benne }
	object { Support_Piston_Benne }
	object { Articulation_Benne translate z*-11 }
	object { Articulation_Benne translate z* 11 }
	}
}

//-----------------------------------------------------------------
