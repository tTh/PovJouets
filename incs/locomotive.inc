/*
		UNE LOCOMOTIVE A VAPEUR
		-----------------------

	necessite l'inclusion des elements communs 'train.inc'

*/

#include   "incs/instruments.inc"

#declare Base_Loco_Vapeur = object
{
#local HGR = 4;
#local HPR = 3;
union
	{
	object { Chassis_Long }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, HPR, -13> }
	object { Petite_Roue 			translate < 5.5, HPR, -13> }
	object { Grande_Roue scale <-1, 1, 1>	translate <-5.5, HGR,  -5> }
	object { Grande_Roue 			translate < 5.5, HGR,  -5> }
	object { Grande_Roue scale <-1, 1, 1>	translate <-5.5, HGR,   5> }
	object { Grande_Roue 			translate < 5.5, HGR,   5> }
	object { Petite_Roue scale <-1, 1, 1>	translate <-5.5, HPR,  13> }
	object { Petite_Roue 			translate < 5.5, HPR,  13> }
	}
}


#declare Dim_Foyer = 3.5;

#declare Foyer = object
{
union	{
	sphere		{ <0, 0, -13>, Dim_Foyer }
	cylinder	{ <0, 0, -13>, <0, 0, 0>, Dim_Foyer }
	cylinder	{ <0, 0, -12>, <0, 0, -11>, Dim_Foyer+0.2 }
	cylinder	{ <0, 0, -4>, <0, 0, -3>, Dim_Foyer+0.2 }

	cylinder	{ <-3.3, -2.75, -16.5>, <-3.3, -2.75, -1>, .6 }
	sphere		{ <-3.3, -2.75, -16.5>, .6 }
	sphere		{ <-3.3, -2.75, -1>, .6 }

	cylinder	{ < 3.3, -2.75, -16.5>, < 3.3, -2.75, -1>, .6 }
	sphere		{ < 3.3, -2.75, -16.5>, .6 }
	sphere		{ < 3.3, -2.75, -1>, .6 }
	}
texture	{
	pigment 	{ color Gray60 }
	finish		{ phong 0.5 }
	}
translate y*10
}

#declare Cheminee = object
{
difference {
	union {
		cylinder	{ <0, 3, -8>, <0, 6.1, -8>, 1 }
		cone		{ <0, 6, -8>, 1, <0, 8, -8> 2 }
		cone		{ <0, 8, -8>, 2, <0, 9, -8> 1 }
		}
	cylinder { <0, 3, -8>, <0, 6.2, -8>, .95 }
	}
texture	{
	pigment		{ color Gray30 }
	finish		{ phong 0.65 }
	}
translate y*10
}

#declare Cabine = object
{
union	{
	difference {
		box { <-5, 0.2, 0.7>,   <5, 11, 0> }
		box { <-4.5, 7.5, 1.1>, <4.5, 10.75, -0.1> }
		}

	// le toit
	union	{
		box { <-5, 11, 14>, <5, 12, 0> }
		box { <-4, 11.9, 13>, <4, 12.1, 1> }
		}

	// la paroi arriere
	difference
		{
		box { <-5, 0, 10>,   <5, 11, 10.7> }
		box { <-4, 5, 11.1>, <4, 10.5, 9.9> }
		}
	}
translate y*6			// XXX
pigment { color Gray80 }
finish		{ phong 0.6 }
}

#declare Reservoirs = object
{
union
	{
	cylinder { <-3, 8.5, 14>, <-3, 11, 14>, 2 }
	cylinder { < 3, 8.5, 14>, < 3, 11, 14>, 2 }
	sphere   { < 3, 11, 14>, 2 }
	sphere   { <-3, 11, 14>, 2 }
	sphere   { < 3, 8.5, 14>, 2 }
	sphere   { <-3, 8.5, 14>, 2 }
	}
// rotate 10*x
texture { T_Chrome_3C }
}

#declare Locomotive_Vapeur = object
{
union
	{
	object { Base_Loco_Vapeur }
	object { Cabine }
	object { Foyer }
	object { Cheminee }
	object { Reservoirs }
	object { Cadran_0 translate <0, 11.5, 1> }
	object { Cadran_0 translate <-2.45, 11.5, 1> }
	object { Cadran_0 translate <2.45, 11.5, 1> }
	}
}

