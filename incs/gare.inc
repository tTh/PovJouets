/*

		La Gare !-)
		-----------

*/

#declare Corps_Gare = object
{
difference
	{
	box { <-600, 0, -200>, <600, 400, 200> }
	box { <-590, 0, -190>, <590, 390, 190> }

	#declare foo = -3;
	#while (foo <= 3)
		#declare px = foo * 165;
		box { <px-42, 120, -222>, <px+42, 275, 222> }
		#declare foo = foo + 1;
	#end
	
	}
pigment { color White }
finish { phong 0.5 }
translate y*100
}

#declare Quai_Gare = object
{
box { <-700, 0, 220>, <700, 100, -420> }

pigment { color White }
finish { phong 0.4 }
}

#include "incs/enseigne.inc"



#declare Gare = object
{
union	
	{
	object { Corps_Gare }
	object { Quai_Gare }
	object { Enseigne scale 15 translate <0, 460, -210>  }
	}
}

/*-----------------------------------------------------
	je suis le raytraceur des lilas.
-----------------------------------------------------*/
