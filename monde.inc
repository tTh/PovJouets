/*
 *
 *
 */
/*------------------------------------------------------------------*/

#declare R_monde = 290; 	/* le rayon du monde */

cylinder {
	<0, -0.2, 0>, 0, R_monde
	pigment	{
		image_map {
			png "picz/plancher.png" interpolate 2
			}
		rotate 		x*90
		translate 	<-0.5, 0, -0.5>
		scale		<250, 1, 250>
		}
	}

#for (foo, 0, 360, 1)
	#local R2 = R_monde - 1.5;
	#local xpos =  R2 * sin(radians(foo));
	#local zpos =  R2 * cos(radians(foo));
	cylinder { <0, 0, 0>, <0, 5, 0>, 0.40
		pigment { color <0.5, 0.5, 0.9> }
		translate <xpos, 0, zpos> }
#end

sky_sphere {
	pigment
		{
		gradient	y
		color_map
			{
			[0	color Khaki		]
			[0.2	color Gray20 		]
			[0.87	color LightBlue		]
			[1	color Khaki		]
			}
		turbulence 0.25
		}
	rotate 50
	scale	0.15
	translate -1
	}

/*------------------------------------------------------------------*/
