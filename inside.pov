
/*
 *	fichier de test pour l'interieur du wagon Corail.
 *	pour plus de details:		oulala@chez.com
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/corail.inc"

object { Wagon_Corail }
camera	{
	location	<-1.6, 12, -19>
	right		image_width/image_height*x
	look_at		<1, 4, 10>
	angle		80
	}

/*------------------------------------------------------------------*/


// light_source { <180, 125, -180> color White }
//light_source { <170, 100, -180> color White }
//light_source { <120, 11, -185> color White }

/*------------------------------------------------------------------*/
