/*
 *
 *	pour plus de details:		oulala@chez.com
 *
 */

#version 3.7;
#include	"contexte.inc"


/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/rails.inc"
#include	"incs/train.inc"
#include	"incs/electric.inc"

union
	{
	object { Un_Rail_Droit }
	object { Locomotive_Electric translate y*2 }
	}

/*------------------------------------------------------------------*/

camera
	{
	right		x*image_width/image_height
	location	<39, 11.1, -44>
	look_at		<0, 8, 0>
	angle		ANGLE_CAM
	}

light_source { <17, 8, -54> color Gray80 }
// light_source { <150, 130, -150> color Green }
light_source { <270, 130, 250> color Gray50 }

/*------------------------------------------------------------------*/
