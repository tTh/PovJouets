/*
 *
 *	pour plus de details:
 *		http://la.buvette.org/POV/jouets/
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/electric.inc"
#include	"incs/rails.inc"
#include	"incs/benne.inc"
#include	"incs/citerne.inc"
#include	"incs/ridelles.inc"
#include	"incs/corail.inc"
#include	"incs/locomotive.inc"

#declare Trois_rails = object
{
union	{
	object { Un_Rail_Droit }
	object { Un_Rail_Droit translate z*-100 }
	object { Un_Rail_Droit translate z* 100 }
	}
}

#declare Train_Electric = object
{
union	{
	object { Locomotive_Electric	translate <0, 2, -10> }
	object { Wagon_Corail		translate <0, 2,  29> }
	}
}

union
	{
	object { Trois_rails }

	object { Locomotive_Vapeur	translate <0, 2, -52> }
	object { Wagon_Citerne		translate <0, 2, -21> }
	object { Wagon_Benne		translate <0, 2,   6> }
	object { Wagon_Ridelles		translate <0, 2,  35> }

	#local XS = 60;
	object { Trois_rails translate x*XS }

	#local K = Cos_01(clock);
	#local DZ = 50 - (K * 85);
	object { Train_Electric		translate <XS, 0, DZ> }
	}

#include  "incs/poteaux.inc"

#declare foo = -300;
#while (foo < 300)
	object { Poteau_1 translate <25, 0, foo> }
	#declare foo = foo + 42;
#end

/*------------------------------------------------------------------*/

camera
	{
	location	<260, 38, 25>
	right		image_width/image_height*x
	look_at		<0, 3, -10>
	angle		ANGLE_CAM
	}

light_source { <150, 98, -150> color Orange }
light_source { <170, 88, -170> color Yellow }

// light_source { <100, 100, 50> color White }

/*------------------------------------------------------------------*/
