
/*
 *			UN TRAIN SANS GARE ?
 *				NON !
 *
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/
// #include 	"incs/plancher.inc"

#include	"incs/decor.inc"
#include	"incs/gare.inc"
#include	"incs/rails.inc"

object { Gare	}

/*------------------------------------------------------------------*/

camera
	{
	location	<320, 490, -1400>
	look_at		<0, 460, 0>
	}

light_source { <1650, 878, -850> color White }
light_source { <930, 598, -1350> color Yellow }

/*------------------------------------------------------------------*/
