#--------------------------------------------------------

PETIT = +w800 +h600
MOYEN = +w1280 +h1024
GRAND = +w1600 +h1200
DIMS=$(MOYEN)

OPTS = +a +q9 -d

POVRAY = povray

# nouveau mars 2021
INCS = incs

# CJPEGOPT=-quality 88 -progressive -dct float -targa
CJPEGOPT = -progressive -dct float -quality 88
PNGOPT = -interlace -compression 9 -text png.txt

all:	loco.png parking.png corail.png train.png essai.png	\
	inside.png benne.png electric.png truc.png wagons.png	\
	ridelles.png rotonde.png grue.png 			\
	citerne.png voiture.png gare.png

GLOBDEPS=${INCS}/train.inc ${INCS}/poteaux.inc			\
	${INCS}/electric.inc ${INCS}/voiture.inc		\
	${INCS}/rails.inc ${INCS}/grue.inc ${INCS}/ridelles.inc	\
	${INCS}/instruments.inc	${INCS}/corail.inc		\
	${INCS}/citerne.inc ${INCS}/benne.inc			\
	${INCS}/constantes.inc ${INCS}/locomotive.inc		\
	${INCS}/enseigne.inc					\
	picz/plancher.png picz/cadran.png			\
	contexte.inc monde.inc

#--------------------------------------------------------

essai.png:	essai.pov Makefile $(GLOBDEPS)
	$(POVRAY) +Iessai  $(DIMS) $(OPTS)

foo.gif:	Makefile essai.png voiture.png
	convert -delay 20 	 			\
		voiture.png essai.png foo.gif

#-----------------------------------------------------------------------
#
#	le grand garage de tout les wagons
#

rotonde.png: rotonde.pov Makefile $(GLOBDEPS)
	$(POVRAY) +Irotonde  $(DIMS) $(OPTS) +v 

parking.png: parking.pov Makefile $(GLOBDEPS)
	$(POVRAY) +Iparking  $(DIMS) $(OPTS) +v 

wagons.png: wagons.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Iwagons  $(DIMS) $(OPTS) +v

gare.png:	gare.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Igare  $(DIMS) $(OPTS) +v 

#-----------------------------------------------------------------------

loco.png:	loco.pov ${GLOBDEPS} Makefile 
	$(POVRAY) +Iloco  $(DIMS) $(OPTS) +v 

train.png:	train.pov ${GLOBDEPS} Makefile 
	$(POVRAY) +Itrain  $(DIMS) $(OPTS) +v 

corail.png:	corail.pov  Makefile $(GLOBDEPS)
	$(POVRAY) +Icorail  $(DIMS) $(OPTS) +v 

#-----------------------------------------------------------------------

inside.png: inside.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Iinside  $(DIMS) $(OPTS) +v

benne.png: benne.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Ibenne  $(DIMS) $(OPTS) +v

electric.png: electric.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Ielectric  $(DIMS) $(OPTS) +v

citerne.png: citerne.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Iciterne  $(DIMS) $(OPTS) +v

truc.png: truc.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Itruc  $(DIMS) $(OPTS) +v

ridelles.png: ridelles.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Iridelles  $(DIMS) $(OPTS) +v

grue.png: grue.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Igrue  $(DIMS) $(OPTS) +v

#--------------------------------------------------------
#	les trucs en travaux

voiture.png:	voiture.pov $(GLOBDEPS) Makefile
	$(POVRAY) +Ivoiture  $(DIMS) $(OPTS) +v

#--------------------------------------------------------

SRCFILES = *.inc incs/*.inc *.pov *.txt Makefile tools/*.sh

lines:
	wc $(SRCFILES) | sort -n

tarball:   $(SRCFILES)
	ls $^ > MANIFEST ;                              \
	( cd .. ;                                       \
	tar zcvf jouets-tth.tar.gz                   \
	        `sed 's/^/PovJouets\//' PovJouets/MANIFEST` )
	date >> tarball

#--------------------------------------------------------

