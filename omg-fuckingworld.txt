TODO
====

[ ]	Rendre le Makefile pilotable depuis le shell

[ ]	Tester le module à partir d'un .INI de Povray

[ ]	Restructurer l'arborescence de la construction

[ ]	Définir un jeu de textures spécifique

[ ]	Préparer le concept de "mise en cadre"

