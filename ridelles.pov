#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/rails.inc"

object { Un_Rail_Droit  }
object { Un_Rail_Droit  translate z*-100 }
object { Un_Rail_Droit  translate z* 100 }

#include	"incs/ridelles.inc"

object { Wagon_Ridelles translate <0, 2, 0> }


/*------------------------------------------------------------------*/

camera
	{
	location	<40, 15, -50>
 	right     	x*image_width/image_height
	look_at		<0, 5, 0>
	angle 		ANGLE_CAM
	}

light_source { < 200,  70, -200>  color Gray50 }
light_source { <-200, 170, -150>  color White }

/*------------------------------------------------------------------*/
