/*
	quelques elements des voitures
*/
#version 3.7;
#include	"contexte.inc"

#include	"metals.inc"

#include 	"incs/train.inc"
#include	"incs/rails.inc"
#include 	"incs/benne.inc"
#include        "incs/grue.inc"
// #include	"decor.inc"

object { Boogie }

camera {
	location	<25.5, 15, 22>
	look_at		<0, 2, 0>
	angle		ANGLE_CAM
	}

object { Repere }

light_source { <90, 131, 190> color Cyan }
light_source { <90, 111, 250> color Magenta }

/*------------------------------------------------------------------*/
