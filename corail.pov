
/*
 *
 *	pour plus de details:		oulala@chez.com
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/rails.inc"
#include	"incs/corail.inc"
#include	"incs/poteaux.inc"

object 
{
union
	{
	object { Wagon_Corail translate y*2 }
	object { Un_Rail_Droit }
	object { Un_Rail_Droit translate z*100 }
	object { Un_Rail_Droit translate z*200 }
	object { Un_Rail_Droit translate z*300 }
	}
}

#declare foo = -300;
#while (foo < 400)
	object { Poteau_1 translate <20, 0, foo> }
	#declare foo = foo +42;
#end

/*------------------------------------------------------------------*/

camera
	{
	location	<38, 10, -45>
	right		image_width/image_height*x
	look_at		<-5.3, 7, 0>
	angle		ANGLE_CAM
	}

light_source { <550, 725, -900> color Gray60 }
light_source { <600, 700, -850> color Gray50 }


/*------------------------------------------------------------------*/
