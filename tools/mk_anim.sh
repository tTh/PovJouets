#!/bin/bash

SPOOL="$HOME/TMP"
mkdir "$SPOOL"

NBRE=599
POVOPT=" -w1280 -h960 +q9 -a -d "
SRCFILE="train.pov"

for idx in $(seq 0 $NBRE)
do

	outfile=$(printf "%s/%04d.png" $SPOOL $idx)
	horloge=$(echo "$idx / $NBRE" | bc -l)

	printf "%-20s    %.4f\n" $outfile $horloge

	povray -i${SRCFILE} ${POVOPT} -K${horloge} -o${outfile}

done

ffmpeg  -nostdin                                                \
        -loglevel error                                         \
        -y -r 25 -f image2 -i $SPOOL/%04d.png                     \
        -c:v libx264 -pix_fmt yuv420p                           \
        $SPOOL/foo.mp4
