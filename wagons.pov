/*
 *		tous les petits jouets ensembles
 *		--------------------------------
 *
 *	les dimensions sont en centimetres, et on regardera
 *	les choses a une hauteur de 40 cm. 
 *
 *	pour plus de details:		oulala@chez.com
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

/*------------------------------------------------------------------*/

#include	"incs/train.inc"
#include	"incs/citerne.inc"
#include	"incs/ridelles.inc"

object { Wagon_Ridelles	rotate y*-10 translate x*-14 }
object { Wagon_Citerne	rotate y*50 translate x*16 }

#include	"incs/poteaux.inc"

object { Poteau_0 }
object { Poteau_1 translate -z*12 }

/*------------------------------------------------------------------*/

camera
	{
	location	<14, 10, -80>
	right		image_width/image_height*x
	look_at		<0, 7, 0>
	angle		ANGLE_CAM
	}

light_source { <-50, 120, -330> color Gray50 }
light_source { <190, 498, -250> color White }
light_source { <160, 99, -450> color Yellow }

/*------------------------------------------------------------------*/
