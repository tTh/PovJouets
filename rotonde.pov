/*
 *		le grand garage des trains
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/
#include   "incs/poteaux.inc"
object { Poteau_1 }

#include   "incs/rotonde.inc"
object { Rotonde_0 }

#include   "incs/rails.inc"
object { Un_Rail_Droit translate z*-100}

/*------------------------------------------------------------------*/

camera
	{
	location	<20, 70, 240>
	right		image_width/image_height*x
	look_at		<0, 24, 0>
	angle		ANGLE_CAM
	}

light_source { <142, 500, 500> color White }

/*------------------------------------------------------------------*/
