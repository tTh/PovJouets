/*
 *
 *		LOCO.POV
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/rails.inc"
#include	"incs/train.inc"
#include	"incs/locomotive.inc"
#include	"incs/electric.inc"

union {
	object { Un_Rail_Droit }
	object { Un_Rail_Droit translate z*-100 }
	object { Locomotive_Vapeur translate <0, 2, 17.8> }
	rotate y*5
	}


#if (0)
union {
	object { Un_Rail_Droit }
	object { Un_Rail_Droit translate z*-100 }
	object { Locomotive_Electric translate y*2 }
	translate x*42
	}
#end

/*------------------------------------------------------------------*/

camera {
	location	<-72, 17.5, 32>
	right		image_width/image_height*x
	look_at		<5, 9.7, 18>
	angle		ANGLE_CAM
	}

light_source { <150, 118, -150> color Gray70 }
light_source { <-170, 298, -250> color Gray70 }

/*------------------------------------------------------------------*/
