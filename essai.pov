/*
	essai.pov -- un fichier de test.
*/

#version 3.7;
#include	"contexte.inc"

#include	"metals.inc"

#include 	"incs/train.inc"
#include	"incs/rails.inc"
#include 	"incs/benne.inc"
#include        "incs/grue.inc"

// object { Boogie }
object { Grande_Roue }

camera {
	location	<5.5, 15, 20>
	right		image_width/image_height*x
	look_at		<0, 3, 0>
	angle		ANGLE_CAM
	}

object { Repere }

light_source { <90, 131, 190> color White }
light_source { <90, 111, 250> color White }

/*------------------------------------------------------------------*/
