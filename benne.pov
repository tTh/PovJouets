/*

	un fichier de test.
	poser le wagon benne sur les rails.
*/

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include 	"incs/train.inc"
#include	"incs/rails.inc"
#include 	"incs/benne.inc"
#include	"incs/decor.inc"

object { Balise_0 translate x*95 }

object { Un_Rail_Droit }

object { Wagon_Benne translate y*2 }

/*------------------------------------------------------------------*/

#include	"monde.inc"

camera
	{
	location	<35.5, 21, 40>
	right		image_width/image_height*x
	look_at		<0, 6, 0>
	angle		ANGLE_CAM
	}

light_source { <90, 131, 190> color White }
light_source { <90, 111, 250> color White }

/*------------------------------------------------------------------*/
