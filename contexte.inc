/*
 *		contexte.inc
 */

global_settings {
	assumed_gamma 1.0
	ambient_light rgb <0.18, 0.15, 0.17>
	max_trace_level 17
	}

/*------------------------------------------------------------------*/
/*
 *		essential system includes
 */
#include	"colors.inc"
#include	"metals.inc"
#include	"textures.inc"

/*------------------------------------------------------------------*/

#declare	ANGLE_CAM = 42;

//---------------------------------------
/*
 *	deux macros pour des variations plus 'touchy'
 */
#macro Cos_01( X )
  (0.5-0.5*cos( 3.141592654 * X))
#end
#macro Cos_010( X )
  (0.5-0.5*cos( 2 * 3.141592654 * X))
#end
//---------------------------------------

/*------------------------------------------------------------------*/
/*
 *		outils de mise au point (smart tools)
 */
#declare Repere = object
{
#local	DA = 5;
#local	DB = DA * 5;
union	{
	cylinder { <-DA, 0, 0>, <100, 0, 0>, 0.1 pigment { color Red } }
	cylinder { <0, -DA, 0>, <0, 100, 0>, 0.1 pigment { color Green } }
	cylinder { <0, 0, -DA>, <0, 0, 100>, 0.1 pigment { color Blue } }
	}
}
/*------------------------------------------------------------------*/
