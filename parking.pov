/*
 *		Les jouets de tTh - 2021
 *	pour plus de details:		oulala@chez.com
 *
 */

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include	"monde.inc"

#include	"incs/train.inc"
#include	"incs/locomotive.inc"
#include	"incs/electric.inc"
#include	"incs/corail.inc"
#include	"incs/citerne.inc"
#include	"incs/ridelles.inc"
#include 	"incs/rails.inc"
#include	"incs/benne.inc"
#include	"incs/grue.inc"

union {
	object { Wagon_Benne	translate y*2	}
	object { Un_Rail_Droit }
	translate x*-56 
}

union {
	object { Locomotive_Vapeur	translate y*2 }
	object { Un_Rail_Droit }
	translate x*-32 
}

union {
	object { Wagon_Corail	 translate y*2	}
	object { Un_Rail_Droit }
	translate x*-9 
}

union {
	object { Wagon_Ridelles	 translate y*2	}
	object { Un_Rail_Droit }
	translate x*10
}

union {
	object { Wagon_Citerne	translate y*2	}
	object { Un_Rail_Droit }
	translate x*35 
}

union {
	object { Locomotive_Electric	 translate y*2 }
	object { Un_Rail_Droit }
	translate x*57 
}

union {
	object { Wagon_Grue		 translate y*2 }
	object { Un_Rail_Droit }
	translate x*79 
}
/*------------------------------------------------------------------*/

camera
	{
	location	<44, 4, -110>
	right		image_width/image_height*x
	look_at		<10, 1.9, 0>
	angle		ANGLE_CAM
	}

light_source { <150, 78, -250> color Gray20 }
light_source { <140, 140, -250> color Gray20 }
light_source { <-140, 210, -250> color Blue }

/*------------------------------------------------------------------*/
