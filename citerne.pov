/*
 *	une citerne pour transporter le pinard
*/

#version 3.7;
#include	"contexte.inc"

/*------------------------------------------------------------------*/

#include 	"incs/train.inc"
#include	"incs/rails.inc"
#include 	"incs/citerne.inc"
#include	"incs/decor.inc"

object { Un_Rail_Droit }
object { Wagon_Citerne translate y*2 }

/*------------------------------------------------------------------*/

#include	"monde.inc"

camera
	{
	location	<46, 13, -41>
	right		image_width/image_height*x
	look_at		<0, 5.8, 0>
	angle		ANGLE_CAM
	}

light_source { <290, 131, -190> color White }
light_source { <290, 111, -250> color White }

/*------------------------------------------------------------------*/
